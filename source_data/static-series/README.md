# Static series

Here reside CSV files forged from several sources. These data have been processed once and
are not intended to be automatically updated.

## Format

CSV files

- use ',' as delimiter
- contain 3 columns:
  - country
  - year
  - value
- country column contain iso-3166 alpha-3 codes

## Index

`index.csv` file maintain correspondence between CSV data file and indicator slug

## Sources

### `transferts` indicator

Original data downloaded from [WorldBank query](https://datacatalog.worldbank.org/search?search_api_views_fulltext_op=AND&query=+Remittance+inflows+to+GDP&sort_by=search_api_relevance&sort_by=search_api_relevance)

### `deforestation` indicator

Original data downloaded from [WorldBank query](https://datacatalog.worldbank.org/search?search_api_views_fulltext_op=AND&query=Annual+deforestation+%28%25+of+change%29&sort_by=search_api_relevance&sort_by=search_api_relevance)
