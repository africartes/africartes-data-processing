#!/usr/bin/env python3
"""Process indicators and generate ready-to-play content."""

import argparse
import csv
import decimal
import enum
import hashlib
import logging
import pickle
import re
import sys
from collections import OrderedDict, defaultdict
from itertools import chain
from operator import itemgetter
from pathlib import Path
from typing import Dict, List, Optional, Set, Tuple

import country_list
import daiquiri
import dbnomics
import numpy as np
import pandas as pd
import seaborn as sb

import ujson as json
from pycountry import countries
from ruamel.yaml import YAML

DBNOMICS_API_URL = "https://api.db.nomics.world/v22/"
MAX_NB_SERIES = 300

SOURCE_DATA = Path("source_data")
DBNOMICS_CACHE_FILE_NAME = "dbnomics.json"

NA_VALUE = "NA"

CONTINUOUS_SCALE_INTERVAL_RE = re.compile(r"^([^\s]+)\s*[–-]\s*([^\s:]+)(:(.+))?$")
DISCRETE_SCALE_INTERVAL_RE = re.compile(r"^([\d]+):(.+)$")
INDICATOR_FILENAME_RE = re.compile(r"^([A-Z_]+)_indicators.json")

logger = logging.getLogger(__name__)

DimensionFilter = Dict[str, List[str]]


def load_country_info_map(csv_file: Path) -> Dict[str, Dict[str, str]]:
    """Load africa iso-3166 3-letter codes from csv file."""
    with csv_file.open("rt", encoding="utf-8") as fd:
        reader = csv.DictReader(fd)
        return {row["country_code"]: row for row in reader}


class DataFrameCache:
    def __init__(self, cache_dir: Path, params: Dict[str, str]):
        self.params = params
        self.cache_file = cache_dir / f"{self._compute_hash()}.pickle"

    def _compute_hash(self):
        h = hashlib.sha1()
        h.update(
            json.dumps(
                self.params, sort_keys=True, ensure_ascii=False, indent=0
            ).encode("utf-8")
        )
        return h.hexdigest()

    def load(self):
        cache_file = self.cache_file
        if cache_file.is_file():
            logger.debug("Loading DataFrame from cache file %r", str(cache_file))
            try:
                return pd.read_pickle(cache_file)
            except pickle.UnpicklingError:
                logger.exception(
                    "Could not load cache file %r, ignoring it", str(cache_file)
                )
        return None

    def save(self, df: pd.DataFrame):
        cache_file = self.cache_file
        logger.debug("Saving DataFrame to cache file %r", str(cache_file))
        df.to_pickle(cache_file)


def process_dbnomics_series(
    dataset_info: Dict[str, str],
    indicator_code: str,
    dimension_filter: DimensionFilter,
    african_country_info_map: Dict[str, Dict[str, str]],
    op: str,
    target_dir: Path,
    cache_dir: Path,
):
    """Generate world, africa and africa level indicators data."""

    def check_code_format(country_code_format: str) -> str:
        """Check if given code format is one of 'alpha_2', 'alpha_3' or 'numeric'."""
        return (
            country_code_format
            if country_code_format in ("alpha_2", "alpha_3", "numeric")
            else None
        )

    def to_alpha_3(country_code_format: str, code: str) -> str:
        """Convert given code in given code format to alpha_3 code."""
        return countries.get(**{country_code_format: code}).alpha_3

    # Get country dimension name and format : alpha_3, alpha_2, ...
    country_dimension_name = dataset_info.get("country_dimension", "country")
    country_code_format = check_code_format(dataset_info.get("country_code_format"))
    if country_code_format is None:
        raise ValueError(f"Unknown code format: [{country_code_format}]")

    provider_code = dataset_info["provider_code"]
    dataset_code = dataset_info["dataset_code"]

    df_cache_dir = cache_dir / "dbnomics_data"
    df_cache_dir.mkdir(exist_ok=True)
    fetch_series_kwargs = {
        "api_base_url": DBNOMICS_API_URL,
        "provider_code": provider_code,
        "dataset_code": dataset_code,
        "dimensions": dimension_filter,
        "max_nb_series": MAX_NB_SERIES,
    }
    df_cache = DataFrameCache(df_cache_dir, fetch_series_kwargs)
    df = df_cache.load()
    if df is None:
        logger.debug("Loading DataFrame from DBnomics API...")
        # Query DBnomics
        # Note: It would be perhaps better to filter on country codes before
        # to avoid downloading series we don't need
        # This could be done if we could get dataset whole country code list
        # and filter it with pycountry before fetching series
        df = dbnomics.fetch_series(**fetch_series_kwargs)
        df_cache.save(df)

    # Assert non empty
    if len(df) == 0:
        raise ValueError(
            f"No series found for {provider_code}/{dataset_code} {dimension_filter}"
        )

    # Assert annual periodicity
    freq_value_list = df["@frequency"].unique().tolist()
    if not (len(freq_value_list) == 1 and freq_value_list[0] == "annual"):
        raise ValueError("All series are not annual")

    # Extract year numbers from "period" column
    df["year"] = df["period"].map(lambda dt: dt.year)

    # Normalize country column name
    df["country"] = df[country_dimension_name]

    # Extract valid list of country codes
    valid_countries = [
        countries.get(**{country_code_format: code})
        for code in df["country"].unique().tolist()
    ]
    valid_country_codes = [
        getattr(country, country_code_format) for country in valid_countries if country
    ]

    # Then only retain rows with these country codes
    df = df[df["country"].isin(valid_country_codes)]

    # Normalize country codes to alpha_3 if needed
    if country_code_format != "alpha_3":
        df["country"] = df["country"].map(
            lambda code: to_alpha_3(country_code_format, code)
        )

    # operate additional process on data
    if op:
        if op.startswith("div"):
            divider = int(op.strip().split(" ")[-1])
            df["value"] /= divider

    generate_csv_files_from_dataframe(
        indicator_code, df, african_country_info_map, target_dir
    )

    write_dbnomics_cache(
        cache_dir, provider_code, dataset_code, indicator_code, df, dimension_filter
    )


def generate_csv_files_from_dataframe(
    indicator_slug: str,
    df: pd.DataFrame,
    african_country_info_map: Dict[str, Dict[str, str]],
    target_dir: Path,
):
    """Generate CSV files from dataframe containing 3 columns: year, value, country.

    CSV are generated only for World and Africa levels.
    "Direction régionale" CSV files are generated elsewhere in a second step as
    they can require Africa CSV values to weight aggregations.
    """
    # Keep only valuable data
    df = df[["year", "value", "country"]]

    # Sort
    df = df.sort_values(by=["country", "year"])

    # Create target dataframe from original dataframe
    df2 = pd.DataFrame(index=df["year"].unique())
    for alpha_3_code in df["country"].unique():
        df2[alpha_3_code] = df.loc[df["country"] == alpha_3_code]["value"].to_list()

    # Export as world CSV
    df2.to_csv(target_dir / f"{indicator_slug}_world.csv", index_label="Year")

    # Reduce to only african country columns
    african_column_list = [
        col_name for col_name in df2.columns if col_name in african_country_info_map
    ]
    df2 = df2[african_column_list]
    df2.to_csv(target_dir / f"{indicator_slug}_africa.csv", index_label="Year")


def process_dbnomics_series_list(
    series_process_info_file: Path,
    indicators_info_file: Path,
    african_country_info_map: Dict[str, Dict[str, str]],
    target_dir: Path,
    cache_dir: Path,
):
    """Generate CSV files for DBnomics series."""
    yaml = YAML(typ="safe")
    for dataset_item in yaml.load(series_process_info_file):

        dataset_process_info = {
            k: v for k, v in dataset_item.items() if k != "indicators"
        }

        # Indicator
        for indicator_process_info in dataset_item.get("indicators", []):
            indicator_code = indicator_process_info.get("code")
            logger.info("Generate [%s] indicator CSV files...", indicator_code)

            # Normalize dimension_filter from YAML:
            # ensure each dimension has a list of values.
            dimension_filter = {
                k: v if isinstance(v, list) else [v]
                for k, v in indicator_process_info.get("dimension_filter", {}).items()
            }

            try:
                process_dbnomics_series(
                    dataset_process_info,
                    indicator_code,
                    dimension_filter,
                    african_country_info_map,
                    indicator_process_info.get("op"),
                    target_dir,
                    cache_dir,
                )
            except ValueError:
                logger.exception(
                    "An exception occurred during processing, skipping indicator"
                )


def compute_color_list(
    theme_color_name: str, scale_count: str, scale_colors: str, scale_type: str
):
    """Compute scale color list from several params."""

    # discrete scale: colors to use are provided
    if scale_type == "discrete":
        color_list = scale_colors.split(",")
        if len(color_list) != scale_count:
            raise ValueError(
                f"colors '{scale_colors}' length doesn't match {scale_count}"
            )
        return color_list

    # continuus scale:
    if scale_type == "continuous":

        # Only handle a subset of colors
        if theme_color_name not in ("red", "green", "blue"):
            raise ValueError(f"Can't deal with this color: [{theme_color_name}]")

        # Convert color name into seaborn dark palette name
        # 'blue' -> 'Blues_d'
        # see https://seaborn.pydata.org/tutorial/color_palettes.html#sequential-color-palettes # noqa
        palette = f"{theme_color_name.title()}s_d"

        # Generate ['#rrggbb', ...]
        sb_hex_colors = reversed(sb.color_palette(palette, scale_count).as_hex())
        # filter out heading '#'
        return list(map(lambda color: color[1:], sb_hex_colors))

    raise ValueError(f"Unknown scale type: [{scale_type}]")


def compute_scale_info(row):
    """Group scale metadata under a single "scale" property."""
    scale_count = len(row["_scale"].split(","))
    country_color_list = ",".join(
        compute_color_list(
            row["_scale_theme_color"],
            scale_count,
            row["_scale_colors"],
            row["_scale_type"],
        )
    )

    scale_info = {
        "type": row["_scale_type"],
        "country_brackets": list(
            iter_scale(row["_scale"], country_color_list, row["_scale_type"])
        ),
    }

    # Compute DR scale if needed.
    if row["_aggregate_method"] != "no":
        dr_scale_count = (
            len(row["_dr_scale"].split(",")) if row["_dr_scale"] else scale_count
        )

        dr_color_list = ",".join(
            compute_color_list(
                row["_scale_theme_color"],
                dr_scale_count,
                row["_scale_colors"],
                row["_scale_type"],
            )
        )
        dr_scale = row["_dr_scale"] if row["_dr_scale"] else row["_scale"]
        scale_info["dr_brackets"] = list(
            iter_scale(dr_scale, dr_color_list, row["_scale_type"])
        )

    return scale_info


def generate_indicators_json(csv_file: Path, json_output_file: Path, dbnomics_cache):
    """Generate indicators.json file."""
    category_map = OrderedDict()
    indicator_list = []

    with csv_file.open("rt", encoding="utf-8") as fd:
        reader = csv.DictReader(fd)
        for row in reader:
            if row["slug"] == "" or row["_scale"] == "":
                continue
            cat_name = row["category_name"]
            cat_slug = row["category_slug"]
            if cat_name not in category_map:
                category_map[cat_name] = cat_slug

            indicator_info = {
                k: v.strip()
                for k, v in row.items()
                if k not in {"category_name"}
                and not k.startswith("_")
                and v.strip() != ""
            }

            indicator_info["scale"] = compute_scale_info(row)

            indicator_info["category_slug"] = category_map[cat_name]
            indicator_info["area_slugs"] = sorted(
                ["world", "africa"]
                + ([] if row["_aggregate_method"] == "no" else ["africa_dr"])
            )

            # Add source metadata, in particular to allow displaying links to DBnomics.
            dbnomics_cache_for_indicator = get_indicator_from_dbnomics_cache(
                dbnomics_cache, row["slug"]
            )
            source_type = "dbnomics" if dbnomics_cache_for_indicator else "static"
            source_info = {
                "name": row["source"],
                "type": source_type,
            }
            if source_type == "dbnomics":
                source_info.update(dbnomics_cache_for_indicator)
            indicator_info["source"] = source_info

            indicator_list.append(indicator_info)

    indicators_info_dict = {
        "categories": [{"slug": v, "name": k} for k, v in category_map.items()],
        "indicators": indicator_list,
    }

    write_json_file(json_output_file, indicators_info_dict)


def generate_territory_info_json(
    africa_countries_info_map: Dict[str, Dict[str, str]],
    json_output_file: Path,
    data_territory_codes: Set[str],
):
    """Generate territory_info.json file."""

    def iter_world_countries(data_territory_codes: Set[str]):
        fr_countries_map = dict(country_list.countries_for_language("fr"))
        for country in countries:
            # all countries but african ones
            if country.alpha_3 in africa_countries_info_map:
                continue
            # skip country without data
            if country.alpha_3 not in data_territory_codes:
                logger.debug("Skip world %r", country.alpha_3)
                continue

            fr_country_name = fr_countries_map.get(country.alpha_2)
            if fr_country_name:
                yield {
                    "type": "country",
                    "code": country.alpha_3,
                    "name": fr_country_name,
                }

    def iter_africa_countries(data_territory_codes: Set[str]):
        for country, country_info in africa_countries_info_map.items():
            # skip country without data
            if country not in data_territory_codes:
                logger.debug("Skip africa %r", country)
                continue
            yield {
                "type": "country",
                "code": country,
                "name": country_info["country_name"],
            }

    def iter_africa_dr(data_territory_codes: Set[str]):
        dr_map = {}
        for country, country_info in sorted(africa_countries_info_map.items()):
            dr_code, dr_name = country_info["dr_code"], country_info["dr_name"]
            # skip dr without data
            if dr_code not in data_territory_codes:
                logger.debug("Skip dr %r", dr_code)
                continue
            if dr_code not in dr_map:
                dr_map[dr_code] = {
                    "code": dr_code,
                    "name": dr_name,
                    "type": "DR",
                    "country_codes": [],
                }
            dr_map[dr_code]["country_codes"].append(country)
        for v in dr_map.values():
            yield v

    africa_countries = list(iter_africa_countries(data_territory_codes))
    africa_dr = list(iter_africa_dr(data_territory_codes))
    world_countries = list(iter_world_countries(data_territory_codes))

    af_continent = {"code": "AF", "name": "Afrique", "type": "continent"}

    territory_info = {
        "areas": {
            "africa": sorted(country["code"] for country in africa_countries),
            "africa_dr": sorted(dr["code"] for dr in africa_dr),
            "world": sorted(country["code"] for country in world_countries),
        },
        "territories": sorted(
            world_countries + africa_countries + africa_dr + [af_continent],
            key=itemgetter("code"),
        ),
    }

    write_json_file(json_output_file, territory_info)


def aggregate(
    df: pd.DataFrame,
    agg_code: str,
    country_code_list: List[str],
    aggregate_function: str,
    weighting_df: Optional[pd.DataFrame],
):
    """Shortcut to aggregate column values in new column.

    Note: df and weighting_df have to be indexed by Year
    """
    # sum
    if aggregate_function == "sum":
        df[agg_code] = df[country_code_list].sum(axis=1, min_count=1)
        return

    # average
    if aggregate_function == "average":
        df[agg_code] = df[country_code_list].mean(axis=1)
        return

    # weighting
    if aggregate_function == "weighting":

        # Adjust data (same country_code_list)
        d_df = df.loc[:, country_code_list]
        w_df = weighting_df.loc[:, country_code_list]

        period_list = df.index
        series_values = []
        for p in period_list:

            # Handle na using mask
            obs_series = d_df.loc[p, country_code_list]
            weight_series = w_df.loc[p, country_code_list]
            mask = (obs_series.isnull() | weight_series.isnull()).tolist()

            # True values in mask means "skip this value during computation"
            if all(mask):
                series_values.append(np.nan)
                continue

            # Compute weighting
            a = np.ma.array(obs_series.tolist(), mask=mask)
            series_values.append(np.ma.average(a, weights=weight_series.tolist()))

        df[agg_code] = pd.Series(data=series_values, index=period_list)
        return

    raise ValueError(f"Unknown aggregation function: {aggregate_function}",)


def aggregate_info_iter(csv_file: Path):
    """Extract information on aggregating indicators.

    Yield tuples (slug, aggregate method, weighting_source).
    """
    with csv_file.open("rt", encoding="utf-8") as fd:
        reader = csv.DictReader(fd)
        for row in reader:
            yield (row["slug"], row["_aggregate_method"], row["_weighting_source"])


def generate_direction_regionale_csv(
    african_country_info_map: Dict[str, Dict[str, str]],
    indicators_info_csv_file: Path,
    data_dir: Path,
    target_dir: Path,
):
    """Generate indicators data at 'direction régionale' level."""

    # TODO: Peut-on retirer le Sahara occidental (ESH) de african country info map ?

    # Browse indicators
    for indicator_slug, agg_function, weighting_source in aggregate_info_iter(
        indicators_info_csv_file
    ):

        logger.info("Aggregating [%s] indicator", indicator_slug)

        indicator_data_file = data_dir / f"{indicator_slug}_africa.csv"
        if not indicator_data_file.exists():
            logger.error("> No data found!")
            continue

        if agg_function == "no":
            logger.info("> Nothing to do")
            continue

        if agg_function not in ("sum", "weighting", "average"):
            logger.error("> Unknown aggregation function: [%s]", agg_function)
            continue

        weighting_df = None
        if agg_function == "weighting":
            weighting_source = weighting_source.strip()
            if weighting_source == "":
                logger.error("> No weighting source")
                continue
            weighting_data_file = data_dir / f"{weighting_source}_africa.csv"
            if not weighting_data_file.exists():
                logger.error(
                    "> [%s] indicator missing for aggregating", weighting_source
                )
                continue
            weighting_df = pd.read_csv(weighting_data_file, index_col="Year")

        # Load indicator data
        df = pd.read_csv(indicator_data_file, index_col="Year")

        # Intersect df and weighting_df if weighting
        if weighting_df is not None:
            period_list = sorted(
                set(df.index.tolist()) & set(weighting_df.index.tolist())
            )

            # Adjust data (using common period list)
            df = df.loc[
                period_list,
            ]
            weighting_df = weighting_df.loc[
                period_list,
            ]

        # Prepare country groups to merge
        dr_map = defaultdict(list)
        for country_code, country_info in african_country_info_map.items():
            if country_code in df.columns:
                dr_map[country_info["dr_code"]].append(country_code)

        # Aggregate inplace
        for dr_code, country_code_list in dr_map.items():
            aggregate(df, dr_code, country_code_list, agg_function, weighting_df)

        df = df.loc[:, sorted(dr_map.keys())]
        df.to_csv(target_dir / f"{indicator_slug}_africa_dr.csv", index_label="Year")


def generate_continent_csv(
    african_country_info_map: Dict[str, Dict[str, str]],
    indicators_info_csv_file: Path,
    data_dir: Path,
    target_dir: Path,
):
    """Generate indicators data at 'continent' level."""

    # Browse indicators
    for indicator_slug, agg_function, weighting_source in aggregate_info_iter(
        indicators_info_csv_file
    ):

        logger.info("Aggregating [%s] indicator", indicator_slug)

        indicator_data_file = data_dir / f"{indicator_slug}_world.csv"
        if not indicator_data_file.exists():
            logger.error("> No data found!")
            continue

        if agg_function == "no":
            logger.info("> Nothing to do")
            continue

        if agg_function not in ("sum", "weighting", "average"):
            logger.error("> Unknown aggregation function: [%s]", agg_function)
            continue

        weighting_df = None
        if agg_function == "weighting":
            weighting_source = weighting_source.strip()
            if weighting_source == "":
                logger.error("> No weighting source")
                continue
            weighting_data_file = data_dir / f"{weighting_source}_world.csv"
            if not weighting_data_file.exists():
                logger.error(
                    "> [%s] indicator missing for aggregating", weighting_source
                )
                continue
            weighting_df = pd.read_csv(weighting_data_file, index_col="Year")

        # Load indicator data
        df = pd.read_csv(indicator_data_file, index_col="Year")

        # Intersect df and weighting_df if weighting
        if weighting_df is not None:
            period_list = sorted(
                set(df.index.tolist()) & set(weighting_df.index.tolist())
            )

            # Adjust data (using common period list)
            df = df.loc[
                period_list,
            ]
            weighting_df = weighting_df.loc[
                period_list,
            ]

        # Prepare africa country groups to merge
        continent_map = defaultdict(list)
        for country_code in african_country_info_map:
            if country_code in df.columns:
                continent_map["AF"].append(country_code)

        # For further data aggregating by continents
        # we have to find country_code -> continent code resource
        # as african_country_info_map only list african countries

        # Aggregate inplace
        for continent_code, country_code_list in continent_map.items():
            aggregate(df, continent_code, country_code_list, agg_function, weighting_df)

        df = df.loc[:, sorted(continent_map.keys())]
        df.to_csv(target_dir / f"{indicator_slug}_continent.csv", index_label="Year")


def generate_african_countries_age_distribution_csv(
    african_country_info_map: Dict[str, Dict[str, str]],
    target_dir: Path,
    cache_dir: Path,
):
    """Generate CSV data files for age distribution diagrams."""

    # Prepare numeric / alpha_3 conversion
    country_from_numeric = {}
    for country_code in african_country_info_map:
        country = countries.get(alpha_3=country_code)
        if country is None:
            logger.error("Can't find african country by its code: [%s]", country_code)
            continue
        country_from_numeric[country.numeric] = country
    alpha_3_country_code_list = sorted(country_from_numeric.keys())

    df_cache_dir = cache_dir / "dbnomics_data"
    df_cache_dir.mkdir(exist_ok=True)
    fetch_series_kwargs = {
        "api_base_url": DBNOMICS_API_URL,
        "provider_code": "UNDATA",
        "dataset_code": "DF_UNDATA_WPP",
        "dimensions": {
            "INDICATOR": ["SP_POP_TOTL"],
            "SCENARIO": ["M"],
            "SEX": ["M", "F"],
            "REF_AREA": alpha_3_country_code_list,
        },
        # Note: 44 is the series number by country filtering with above criteria
        "max_nb_series": 44 * len(alpha_3_country_code_list),
    }
    df_cache = DataFrameCache(df_cache_dir, fetch_series_kwargs)
    df = df_cache.load()
    if df is None:
        logger.debug("Loading DataFrame from DBnomics API...")
        # Can be long
        df = dbnomics.fetch_series(**fetch_series_kwargs)
        df_cache.save(df)

    # Assert annual periodicity
    freq_value_list = df["@frequency"].unique().tolist()
    if not (len(freq_value_list) == 1 and freq_value_list[0] == "annual"):
        raise ValueError("All series are not annual")

    # Extract year numbers from "period" column
    df["year"] = df["period"].map(lambda dt: dt.year)

    # Normalize country column name
    df["country"] = df["REF_AREA"].apply(
        lambda num: country_from_numeric.get(num).alpha_3
    )

    df = df[["country", "year", "SEX", "AGE", "value"]]
    df.columns = ["country", "year", "sex", "age", "value"]

    # group by country and dump csv files
    df = df.sort_values(by=["country", "year", "sex", "age"])
    for country, subdf in df.groupby(by="country"):

        logger.info("Generate population pyramid data for [%s]...", country)
        subdf.to_csv(target_dir / f"pop_{country}.csv", index=None)


def process_static_series(
    indicator_slug: str,
    csv_data_file: Path,
    african_country_map: Dict[str, Dict[str, str]],
    target_dir: Path,
):
    """Generate Africartes data CSV from static data."""

    logger.info("Processing static data [%s]", indicator_slug)

    # Load normalized CSV into pandas dataframe
    df = pd.read_csv(csv_data_file)

    # Filter bad country codes
    df_country_code_list = df["country"].unique().tolist()
    ok_country_code_set = set()
    for country_code in df_country_code_list:
        country = countries.get(alpha_3=country_code)
        if country is None:
            logger.warning(
                "Unknown country code [%s] in [%s]", country_code, csv_data_file
            )
        else:
            ok_country_code_set.add(country_code)
    if len(df_country_code_list) > len(ok_country_code_set):
        df = df[df["country"].isin(ok_country_code_set)]

    # Align data to year list
    # adding Na values for missing year observations
    compound_df = pd.DataFrame()
    year_list = sorted(df["year"].unique().tolist())
    df = df.set_index(df["year"])[["country", "value"]]
    country_code_list = sorted(ok_country_code_set)
    for country_code in country_code_list:
        sub_df = df[df["country"] == country_code].reindex(year_list)
        sub_df["country"] = country_code
        sub_df["year"] = sub_df.index
        sub_df = sub_df.reset_index(drop=True)
        compound_df = pd.concat([compound_df, sub_df])

    # Create CSV files
    generate_csv_files_from_dataframe(
        indicator_slug, compound_df, african_country_map, target_dir
    )


def process_static_series_list(
    source_dir: Path,
    african_country_info_map: Dict[str, Dict[str, str]],
    target_dir: Path,
):
    """Generate data CSVs from data already processed."""
    slug_by_filename = {}
    index_filepath = source_dir / "index.csv"
    if not index_filepath.exists():
        logger.error("Index file (index.csv) not found for static series")
        return
    with index_filepath.open("rt", encoding="ascii") as fd:
        reader = csv.DictReader(fd)
        for row in reader:
            slug_by_filename[row["filename"]] = row["slug"]

    csv_data_files = [f for f in source_dir.glob("*.csv") if f.stem != "index"]
    for csv_data_file in csv_data_files:
        indicator_slug = slug_by_filename.get(csv_data_file.name)
        if indicator_slug is None:
            logger.warning("No slug defined for CSV file [%s]", csv_data_file)
            continue
        process_static_series(
            indicator_slug, csv_data_file, african_country_info_map, target_dir
        )


def decimal_from_value(value):
    """Convert string value to decimal.Decimal object."""
    return decimal.Decimal(value) if value else None


def convert_to_country_data(
    source_dir: Path, target_dir: Path,
):
    """Convert indicators data (world level) to country data."""
    total_df = pd.DataFrame()

    # Fill big indicators dataframe for african countries
    logger.info("Merging world CSV files...")
    for csv_file in source_dir.glob("*_world.csv"):
        indicator_slug = csv_file.stem[: csv_file.stem.find("_world")]

        # Read values as Decimal objects to preserve original decimal precision
        col_names = pd.read_csv(csv_file, nrows=0).columns
        converters = {
            col_name: decimal_from_value for col_name in col_names if col_name != "Year"
        }
        df = pd.read_csv(
            csv_file, index_col="Year", dtype={"index": int}, converters=converters
        )

        # Surely not the most efficient way to do it
        data = []
        year_list = df.index
        for country_code in df.columns:
            period_value_list = zip(year_list, df[country_code].tolist())
            row_info = {
                "country": country_code,
                "indicator": indicator_slug,
            }
            for period, value in period_value_list:
                data.append({**row_info, "year": period, "value": value})
        df = pd.DataFrame(data)
        total_df = pd.concat([total_df, df])

    total_df = total_df.sort_values(by=["country", "indicator", "year"])

    # Generate JSON files by countries
    for country_code, sub_df in total_df.groupby(by="country"):
        logger.info("Generating JSON data for country [%s]", country_code)
        indicator_list = sub_df["indicator"].unique().tolist()

        # Gets alpha_2 code from alpha_3
        country = countries.get(alpha_3=country_code)
        if country is None:
            logger.warning("Can't find country from code [%s]", country_code)
            continue
        json_data = {
            "indicators": {
                ind_slug: list(
                    zip(
                        sub_df[sub_df["indicator"] == ind_slug]["year"].tolist(),
                        sub_df[sub_df["indicator"] == ind_slug]["value"]
                        .fillna(NA_VALUE)
                        .tolist(),
                    )
                )
                for ind_slug in indicator_list
            },
            "territory_code": country_code,
        }
        write_json_file(target_dir / f"{country_code}_indicators.json", json_data)


def write_agg_indicators_json(
    agg_code: str, sub_df: pd.DataFrame, target_dir: Path,
):
    """Compute and write aggregated indicators json file."""
    indicator_list = sub_df["indicator"].unique().tolist()
    json_data = {
        "indicators": {
            ind_slug: list(
                zip(
                    sub_df[sub_df["indicator"] == ind_slug]["year"].tolist(),
                    sub_df[sub_df["indicator"] == ind_slug]["value"]
                    .fillna(NA_VALUE)
                    .tolist(),
                )
            )
            for ind_slug in indicator_list
        },
        "territory_code": agg_code,
    }
    write_json_file(target_dir / f"{agg_code}_indicators.json", json_data)


def compute_aggregated_df(
    agg_level_code: str, indicator_item_iterator: List[Tuple[str, Path]]
):
    """Compute aggregated df."""
    total_df = pd.DataFrame()
    for indicator_slug, csv_file in indicator_item_iterator:
        # Read values as Decimal objects to preserve original decimal precision
        col_names = pd.read_csv(csv_file, nrows=0).columns
        converters = {
            col_name: decimal_from_value for col_name in col_names if col_name != "Year"
        }
        df = pd.read_csv(
            csv_file, index_col="Year", dtype={"index": int}, converters=converters
        )

        # Surely not the most efficient way to do it
        data = []
        year_list = df.index.tolist()
        for agg_code in df.columns:
            period_value_list = zip(year_list, df[agg_code].tolist())
            row_info = {
                agg_level_code: agg_code,
                "indicator": indicator_slug,
            }
            for period, value in period_value_list:
                data.append({**row_info, "year": period, "value": value})
        df = pd.DataFrame(data)
        total_df = pd.concat([total_df, df])

    return total_df.sort_values(by=[agg_level_code, "indicator", "year"])


def convert_to_dr_data(source_dir: Path, target_dir: Path):
    """Convert indicators data (dr level) to dr data."""
    # Fill big indicators dataframe for direction regionales
    logger.info("Merging africa_dr CSV files...")
    indicator_items = [
        (csv_file.stem[:-10], csv_file)
        for csv_file in source_dir.glob("*_africa_dr.csv")
    ]
    total_df = compute_aggregated_df("dr", indicator_items)

    # Generate JSON files by dr
    for dr_code, sub_df in total_df.groupby(by="dr"):
        logger.info("Generating JSON data for dr [%s]", dr_code)

        write_agg_indicators_json(dr_code, sub_df, target_dir)


def convert_to_continent_data(source_dir: Path, target_dir: Path):
    """Convert indicators data (continent level) to continent data."""

    # Fill big indicators dataframe for continents
    logger.info("Merging continent CSV files...")
    indicator_items = [
        (csv_file.stem[:-10], csv_file)
        for csv_file in source_dir.glob("*_continent.csv")
    ]
    total_df = compute_aggregated_df("continent", indicator_items)

    # Generate a JSON file by continent
    for continent_code, sub_df in total_df.groupby(by="continent"):
        logger.info("Generating JSON data for continent [%s]", continent_code)

        write_agg_indicators_json(continent_code, sub_df, target_dir)


def get_dbnomics_cache_path(cache_dir: Path) -> Path:
    return cache_dir / DBNOMICS_CACHE_FILE_NAME


def read_dbnomics_cache(cache_dir: Path):
    return read_json_file(get_dbnomics_cache_path(cache_dir))


def get_indicator_from_dbnomics_cache(dbnomics_cache, code: str):
    return dbnomics_cache["indicators"].get(code)


def write_dbnomics_cache(
    cache_dir: Path,
    provider_code: str,
    dataset_code: str,
    indicator_code: str,
    df: pd.DataFrame,
    dimension_filter: DimensionFilter,
):
    indicator_cache = {
        "provider_code": provider_code,
        "dataset_code": dataset_code,
        "series_codes": {
            country_code: series_code
            for (country_code, series_code), _ in df.groupby(
                [df.country, df.series_code]
            )
        },
        "dimension_filter": dimension_filter,
    }

    try:
        dbnomics_cache = read_dbnomics_cache(cache_dir)
    except FileNotFoundError:
        dbnomics_cache = {}

    dbnomics_cache.setdefault("indicators", {})[indicator_code] = indicator_cache

    write_json_file(get_dbnomics_cache_path(cache_dir), dbnomics_cache)


def read_json_file(json_file: Path):
    """Read JSON content from file."""
    with json_file.open() as fd:
        return json.load(fd)


def write_json_file(json_file: Path, data):
    """Write given data as JSON in given file."""
    with json_file.open("wt", encoding="utf-8") as fd:
        json.dump(data, fd, ensure_ascii=False, sort_keys=True, indent=2)


def to_float_or_keep(value):
    """Convert scale string into float when possible.

    >>> to_float_or_keep("3")
    3.0
    >>> to_float_or_keep("-Inf")
    '-Inf'
    >>> to_float_or_keep("Inf")
    'Inf'
    """
    return value if value.endswith("Inf") else float(value)


def iter_scale(scale, scale_colors, scale_type):
    """Iterate on scale info."""
    for item, color in zip(scale.split(","), scale_colors.split(",")):
        if scale_type == "continuous":
            match = re.match(CONTINUOUS_SCALE_INTERVAL_RE, item)
            if match is None:
                raise ValueError(f"Invalid continuous scale item: {item}")
            groups = match.groups()
            result = {
                "min": to_float_or_keep(groups[0]),
                "max": to_float_or_keep(groups[1]),
                "color": color,
            }
            label = groups[3]
            if label is not None:
                result["label"] = label
            yield result
        elif scale_type == "discrete":
            match = re.match(DISCRETE_SCALE_INTERVAL_RE, item)
            if match is None:
                raise ValueError(f"Invalid discrete scale item: {item}")
            groups = match.groups()
            yield {
                "value": int(groups[0]),
                "label": groups[1],
                "color": color,
            }
        else:
            raise NotImplementedError(f"scale_type: {scale_type}")


class ProcessOption(enum.Enum):
    """Atomic process parts."""

    dbnomics = "dbnomics"
    static = "static"
    dr = "dr"
    continent = "continent"
    by_country = "by_country"
    by_dr = "by_dr"
    by_continent = "by_continent"
    pyramids = "pyramids"
    indicators_info = "indicators_info"
    territory_info = "territory_info"

    def __str__(self):
        """Compute string representation."""
        return self.value


def iter_territory_code_from_indicator_dir(indicator_dir: Path):
    """Return territory codes from scanning indicator dir."""
    for json_file in indicator_dir.glob("*_indicators.json"):
        m = INDICATOR_FILENAME_RE.match(json_file.name)
        if m:
            yield m.group(1)


def main():
    """Generate data files for API."""
    parser = argparse.ArgumentParser(usage="Process data for africartes-api")
    parser.add_argument("target_dir", type=Path, help="where to generate data")
    parser.add_argument("--cache-dir", type=Path, help="where to store cache data")
    parser.add_argument("--only", type=ProcessOption, choices=list(ProcessOption))
    parser.add_argument(
        "-v", "--verbose", action="store_true", help="show INFO logging messages"
    )
    parser.add_argument(
        "-d", "--debug", action="store_true", help="show DEBUG logging messages"
    )
    parser.add_argument(
        "--debug-http",
        action="store_true",
        help="show HTTP requests as DEBUG logging messages",
    )
    args = parser.parse_args()

    # Checks target dir
    if not args.target_dir.exists():
        parser.error(f"Target dir {str(args.target_dir)!r} not found!")
    target_dir = args.target_dir

    daiquiri.setup()
    logger.setLevel(
        logging.DEBUG
        if args.debug
        else logging.INFO
        if args.verbose
        else logging.WARNING
    )
    if args.debug_http:
        urllib3_log = logging.getLogger("urllib3")
        urllib3_log.setLevel(logging.DEBUG)

    # Generate cache dir it not given
    if args.cache_dir is None:
        temp_dir = Path("cache")
        temp_dir.mkdir(exist_ok=True)
        logger.debug("--cache-dir not given, using directory %r", str(temp_dir))
        args.cache_dir = temp_dir

    # Get country codes information
    country_filepath = SOURCE_DATA / "africa_countries_info.csv"
    african_country_info_map = load_country_info_map(country_filepath)

    # Process DBnomics series
    csv_dir = target_dir / "csv_data"
    csv_dir.mkdir(exist_ok=True)
    if not args.only or args.only == ProcessOption.dbnomics:
        process_dbnomics_series_list(
            SOURCE_DATA / "dbnomics_series.yml",
            SOURCE_DATA / "indicators_info.csv",
            african_country_info_map,
            csv_dir,
            args.cache_dir,
        )

    # Generate pre-processed series
    if not args.only or args.only == ProcessOption.static:
        process_static_series_list(
            SOURCE_DATA / "static-series", african_country_info_map, csv_dir,
        )

    # Compute 'direction régionale' csv
    if not args.only or args.only == ProcessOption.dr:
        generate_direction_regionale_csv(
            african_country_info_map,
            SOURCE_DATA / "indicators_info.csv",
            csv_dir,
            csv_dir,
        )

    # Compute 'continent' csv
    if not args.only or args.only == ProcessOption.continent:
        generate_continent_csv(
            african_country_info_map,
            SOURCE_DATA / "indicators_info.csv",
            csv_dir,
            csv_dir,
        )

    # Convert indicators data to country-indicators data
    if not args.only or args.only == ProcessOption.by_country:
        country_dir = target_dir / "country_data"
        country_dir.mkdir(exist_ok=True)
        convert_to_country_data(csv_dir, country_dir)

    # Convert indicators data to dr-indicators data
    if not args.only or args.only == ProcessOption.by_dr:
        dr_dir = target_dir / "dr_data"
        dr_dir.mkdir(exist_ok=True)
        convert_to_dr_data(csv_dir, dr_dir)

    # Convert indicators data to cont-indicators data
    if not args.only or args.only == ProcessOption.by_continent:
        cont_dir = target_dir / "continent_data"
        cont_dir.mkdir(exist_ok=True)
        convert_to_continent_data(csv_dir, cont_dir)

    # Generate country age distribution
    if not args.only or args.only == ProcessOption.pyramids:
        pop_target_dir = target_dir / "pop_data"
        pop_target_dir.mkdir(exist_ok=True)
        generate_african_countries_age_distribution_csv(
            african_country_info_map, pop_target_dir, args.cache_dir
        )

    # Generate indicators_info.json
    if not args.only or args.only == ProcessOption.indicators_info:
        try:
            dbnomics_cache = read_dbnomics_cache(args.cache_dir)
        except FileNotFoundError:
            logger.error(
                "Could not find %r file in %s, run the 'dbnomics' step first",
                DBNOMICS_CACHE_FILE_NAME,
                args.cache_dir,
            )
            return 1
        generate_indicators_json(
            SOURCE_DATA / "indicators_info.csv",
            target_dir / "indicators_info.json",
            dbnomics_cache,
        )

    # Gather territory codes from data
    data_territory_codes = set(
        chain(
            iter_territory_code_from_indicator_dir(target_dir / "country_data"),
            iter_territory_code_from_indicator_dir(target_dir / "dr_data"),
            iter_territory_code_from_indicator_dir(target_dir / "continent_data"),
        )
    )

    # Generate territory_info.json
    if not args.only or args.only == ProcessOption.territory_info:
        generate_territory_info_json(
            african_country_info_map,
            target_dir / "territory_info.json",
            data_territory_codes,
        )


if __name__ == "__main__":
    sys.exit(main())
